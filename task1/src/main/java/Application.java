import model.Kotik;

public class Application {
    public static void main(String[] args) {
        Kotik kotik1 = new Kotik("Миса", 8, 6, "Мяу");
        Kotik kotik2 = new Kotik();

        kotik2.setKotik(5, "Стасик", 4, "Мрр...Мяууу");

        kotik1.liveAnotherDay();
        System.out.println("Котик 1: Name - " + kotik1.getName() + ", Вес - " + kotik1.getWeight());

        System.out.println("Котики разговаривают одинаково? " + kotik1.getName().equals(kotik2.getName()));

        System.out.println("Котиков создано: " + Kotik.getInstanceCount());
    }
}