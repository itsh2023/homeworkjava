package model;

import java.util.Random;

public class Kotik {
    private static int instanceCount = 0;

    private String name;
    private int prettiness;
    private int weight;
    private String meow;
    private int satiety;

    public Kotik() {
        instanceCount++;
    }

    public Kotik(String name, int prettiness, int weight, String meow) {
        this();
        this.name = name;
        this.prettiness = prettiness;
        this.weight = weight;
        this.meow = meow;
        this.satiety = 50;
    }

    public void setKotik(int prettiness, String name, int weight, String meow) {
        this.name = name;
        this.prettiness = prettiness;
        this.weight = weight;
        this.meow = meow;
        this.satiety = 50;
    }

    public String getName() {
        return name;
    }

    public int getWeight() {
        return weight;
    }

    public static int getInstanceCount() {
        return instanceCount;
    }

    private boolean Hungry() {
        return satiety <= 0;
    }

    private void getFood() {
        System.out.println(name + " голоден(на).");
        eat();
    }

    public void eat(int foodUnits) {
        satiety += foodUnits;
        System.out.println(name + " кушает.");
    }

    public void eat(int foodUnits, String foodName) {
        eat(foodUnits);
        System.out.println(name + " кушает " + foodName + ".");
    }

    public void eat() {
        eat(10, "корм КитиКат");
    }

    private void decreaseSatiety() {
        satiety -= 10;
    }

    public boolean play() {
        if (Hungry()) {
            getFood();
            return false;
        } else {
            System.out.println(name + " играет на полу с игрушкой.");
            decreaseSatiety();
            return true;
        }
    }

    public boolean sleep() {
        if (Hungry()) {
            getFood();
            return false;
        } else {
            System.out.println(name + " ушел спать.");
            decreaseSatiety();
            return true;
        }
    }

    public boolean chaseMouse() {
        if (Hungry()) {
            getFood();
            return false;
        } else {
            System.out.println(name + " погнался за мышью.");
            decreaseSatiety();
            return true;
        }
    }

    public boolean goForAWalk() {
        if (Hungry()) {
            getFood();
            return false;
        } else {
            System.out.println(name + " пошел гулять в сад.");
            decreaseSatiety();
            return true;
        }
    }

    public boolean talking() {
        if (Hungry()) {
            getFood();
            return false;
        } else {
            System.out.println(name + " лежит на коленях у хозяина и мурлычет.");
            decreaseSatiety();
            return true;
        }
    }

    public boolean lookWindow() {
        if (Hungry()) {
            getFood();
            return false;
        } else {
            System.out.println(name + " любуется видом из окна.");
            decreaseSatiety();
            return true;
        }
    }

    public void liveAnotherDay() {
        Random random = new Random();
        for (int i = 0; i < 24; i++) {
            int action = random.nextInt(6);
            switch (action) {
                case 0:
                    play();
                    break;
                case 1:
                    talking();
                    break;
                case 2:
                    goForAWalk();
                    break;
                case 3:
                    chaseMouse();
                    break;
                case 4:
                    sleep();
                    break;
                case 5:
                    lookWindow();
                    break;
            }
        }
    }
}